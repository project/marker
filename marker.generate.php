<?php

/**
 * @file
 * Generates placeholder marker entities.
 *
 * @usage $ fin drush exec marker_bundle_name number_to_create
 */

/**
 * The marker bundle name.
 *
 * @var string
 */
$bundle = $_SERVER['argv'][3];

/**
 * The number of entities to create.
 *
 * @var int
 */
$limit = $_SERVER['argv'][4];

/**
 * Generate the entities.
 */
/** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
$entity_type_manager = \Drupal::service('entity_type.manager');

for ($i = 1; $i <= $limit; $i++) {
  $entity = $entity_type_manager
    ->getStorage('marker')
    ->create([
      'bundle' => $bundle,
      'name' => "Marker #{$i}",
      'coordinates' => [
        'large' => '0.00000000,0.00000000',
      ],
      'behavior' => 'hover',
    ]);

  $entity->save();
}
