<?php

namespace Drupal\marker\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an item list class for marker entity reference fields.
 */
class MarkerFieldItemList extends EntityReferenceFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state) {
    // Disable configurable default values on marker entity references.
    return [];
  }

}
