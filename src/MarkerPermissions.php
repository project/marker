<?php

namespace Drupal\marker;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for each marker type.
 */
class MarkerPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MarkerPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Returns an array of marker type permissions.
   *
   * @return array
   *   The marker type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function markerTypePermissions() {
    $perms = [];
    // Generate marker permissions for all marker types.
    $marker_types = $this->entityTypeManager
      ->getStorage('marker_type')->loadMultiple();
    foreach ($marker_types as $type) {
      $perms += $this->buildPermissions($type);
    }
    return $perms;
  }

  /**
   * Returns a list of marker permissions for a given marker type.
   *
   * @param \Drupal\marker\MarkerTypeInterface $type
   *   The marker type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(MarkerTypeInterface $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "create $type_id marker" => [
        'title' => $this->t('%type_name: Create new marker', $type_params),
      ],
      "edit own $type_id marker" => [
        'title' => $this->t('%type_name: Edit own marker', $type_params),
      ],
      "edit any $type_id marker" => [
        'title' => $this->t('%type_name: Edit any marker', $type_params),
      ],
      "delete own $type_id marker" => [
        'title' => $this->t('%type_name: Delete own marker', $type_params),
      ],
      "delete any $type_id marker" => [
        'title' => $this->t('%type_name: Delete any marker', $type_params),
      ],
    ];
  }

}
