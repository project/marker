<?php

namespace Drupal\marker;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Marker manager contains common functions to manage marker fields.
 */
class MarkerManager implements MarkerManagerInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Construct the MarkerManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields($entity_type_id, $target_field_name = NULL) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
      return [];
    }
    $map = $this->entityFieldManager->getFieldMapByFieldType('marker');
    $fields = isset($map[$entity_type_id]) ? $map[$entity_type_id] : [];

    if ($target_field_name) {
      foreach ($fields as $field_name => $field_config) {
        /** @var \Drupal\field\Entity\FieldStorageConfig $field */
        $field = $this->entityTypeManager->getStorage('field_storage_config')
          ->load("$entity_type_id.$field_name");

        if ($field->getSetting('field_name') != $target_field_name) {
          // Remove fields not using the targeted field name.
          unset($fields[$field_name]);
        }
      }
    }
    return $fields;
  }

}
