<?php

namespace Drupal\marker\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'marker' field type.
 *
 * @FieldType(
 *   id = "marker",
 *   label = @Translation("Marker"),
 *   description = @Translation("This field manages configuration and presentation of markers on an entity."),
 *   category = @Translation("Reference"),
 *   default_widget = "marker_default",
 *   default_formatter = "marker_default",
 *   list_class = "\Drupal\marker\Field\MarkerFieldItemList",
 * )
 */
class MarkerItem extends EntityReferenceItem {

  /**
   * An array of field types allowed to use markers.
   *
   * @var array
   */
  const ALLOWED_FIELD_TYPES = [
    'image',
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'marker',
      'field_name' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    // Disable changes to the target entity type.
    $element['target_type']['#type'] = 'hidden';
    $element['target_type']['#disabled'] = TRUE;

    // Get all the possible fields in this fields parent entity.
    $entity = $this->getEntity();
    $options = [
      '' => ' - ' . $this->t('Select') . ' - ',
    ];
    foreach ($entity->getFieldDefinitions() as $field_definition) {
      if (in_array($field_definition->getType(), self::ALLOWED_FIELD_TYPES) && !$field_definition->isReadOnly()) {
        $options[$field_definition->getName()] = $field_definition->getLabel();
      }
    }
    if (count($options) === 1) {
      /** @var \Drupal\Core\Entity\EntityTypeBundleInfo $bundle_info */
      $bundle_info = \Drupal::service("entity_type.bundle.info");
      $bundle = $bundle_info->getBundleInfo($this->getEntity()->getEntityTypeId());
      \Drupal::messenger()
        ->addError($this->t('%type does not have a compatible image field. Create an image field and then come back to configure this markers field.', [
          '%type' => $bundle[$this->getEntity()->bundle()]['label'],
        ]));
    }
    $element['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#description' => $this->t('Select the image field to use for this markers field.'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->getSetting('field_name'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);

    unset($form['handler']['#type']);

    // Disable changes to the entity reference handler.
    $form['handler']['handler']['#type'] = 'hidden';
    $form['handler']['handler']['#disabled'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

}
