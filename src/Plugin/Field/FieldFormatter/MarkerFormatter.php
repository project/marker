<?php

namespace Drupal\marker\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Provides a default marker formatter.
 *
 * @FieldFormatter(
 *   id = "marker_default",
 *   module = "marker",
 *   label = @Translation("Markers"),
 *   field_types = {
 *     "marker"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class MarkerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [];
  }

}
