<?php

namespace Drupal\marker\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'marker' entity reference selection.
 *
 * @EntityReferenceSelection(
 *   id = "default:marker",
 *   label = @Translation("Marker selection"),
 *   entity_types = {"marker"},
 *   group = "default",
 *   weight = 0
 * )
 */
class MarkerSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Disable unused entity reference selection options.
    unset($form['auto_create'], $form['sort']);

    return $form;
  }

}
