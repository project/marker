<?php

namespace Drupal\marker;

/**
 * Marker manager contains common functions to manage marker fields.
 */
interface MarkerManagerInterface {

  /**
   * Utility function to return an array of marker fields.
   *
   * @param string $entity_type_id
   *   The entity type to which the marker fields are attached.
   * @param string $target_field_name
   *   The optional target field name.
   *
   * @return array
   *   An array of marker field map definitions, keyed by field name. Each
   *   value is an array with two entries:
   *   - type: The field type.
   *   - bundles: The bundles in which the field appears, as an array with
   *     entity types as keys and the array of bundle names as values.
   */
  public function getFields($entity_type_id, $target_field_name = NULL);

}
