<?php

namespace Drupal\marker;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for the marker edit forms.
 *
 * @internal
 */
class MarkerForm extends ContentEntityForm {

}
