<?php

namespace Drupal\marker;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a marker entity.
 */
interface MarkerInterface extends ContentEntityInterface, EntityChangedInterface, RevisionLogInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the marker item name.
   *
   * @return string
   *   The name of the marker item.
   */
  public function getName();

  /**
   * Sets the marker item name.
   *
   * @param string $name
   *   The name of the marker item.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Returns the marker item creation timestamp.
   *
   * @todo Remove and use the new interface when #2833378 is done.
   * @see https://www.drupal.org/node/2833378
   *
   * @return int
   *   Creation timestamp of the marker item.
   */
  public function getCreatedTime();

  /**
   * Sets the marker item creation timestamp.
   *
   * @todo Remove and use the new interface when #2833378 is done.
   * @see https://www.drupal.org/node/2833378
   *
   * @param int $timestamp
   *   The marker creation timestamp.
   *
   * @return $this
   *   The called marker item.
   */
  public function setCreatedTime($timestamp);

}
