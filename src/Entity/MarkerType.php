<?php

namespace Drupal\marker\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\marker\MarkerTypeInterface;

/**
 * Defines the marker type entity.
 *
 * @ConfigEntityType(
 *   id = "marker_type",
 *   label = @Translation("Marker type"),
 *   label_collection = @Translation("Marker types"),
 *   label_singular = @Translation("marker type"),
 *   label_plural = @Translation("marker types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count marker type",
 *     plural = "@count marker types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\marker\MarkerTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\marker\MarkerTypeForm",
 *       "add" = "Drupal\marker\MarkerTypeForm",
 *       "edit" = "Drupal\marker\MarkerTypeForm",
 *       "delete" = "Drupal\marker\Form\MarkerTypeDeleteForm"
 *     },
 *     "list_builder" = "Drupal\marker\MarkerTypeListBuilder"
 *   },
 *   admin_permission = "administer marker types",
 *   config_prefix = "type",
 *   bundle_of = "marker",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "new_revision",
 *     "status",
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/marker/manage/{marker_type}/delete",
 *     "edit-form" = "/admin/structure/marker/manage/{marker_type}",
 *     "add-form" = "/admin/structure/marker/types/add",
 *     "collection" = "/admin/structure/marker",
 *   }
 * )
 */
class MarkerType extends ConfigEntityBundleBase implements MarkerTypeInterface {

  /**
   * The marker type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The marker type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the marker type.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether marker items should be published by default.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * Default value of the 'Create new revision' checkbox of this marker type.
   *
   * @var bool
   */
  protected $new_revision = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($new_revision) {
    return $this->set('new_revision', $new_revision);
  }

}
