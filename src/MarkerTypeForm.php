<?php

namespace Drupal\marker;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\marker\Entity\MarkerType;
use Drupal\media\MediaSourceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form handler for marker type edit forms.
 *
 * @internal
 */
class MarkerTypeForm extends EntityForm {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The marker manager.
   *
   * @var \Drupal\marker\MarkerManagerInterface
   */
  protected $markerManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('marker'),
      $container->get('marker.manager')
    );
  }

  /**
   * Constructs a MarkerTypeFormController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\marker\MarkerManagerInterface $marker_manager
   *   The marker manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, MarkerManagerInterface $marker_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->markerManager = $marker_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('The human-readable name of this marker type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => 32,
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => [MarkerType::class, 'load'],
      ],
      '#description' => $this->t('A unique machine-readable name for this marker type.'),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $this->entity->getDescription(),
      '#description' => $this->t('Describe this marker type. The text will be displayed on the <em>Add new marker</em> page.'),
    ];

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['workflow'] = [
      '#type' => 'details',
      '#title' => $this->t('Publishing options'),
      '#group' => 'additional_settings',
    ];

    $form['workflow']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default options'),
      '#default_value' => $this->getWorkflowOptions(),
      '#options' => [
        'status' => $this->t('Published'),
        'new_revision' => $this->t('Create new revision'),
      ],
    ];

    $form['workflow']['options']['status']['#description'] = $this->t('Marker will be automatically published when created.');
    $form['workflow']['options']['new_revision']['#description'] = $this->t('Automatically create new revisions. Users with the "Administer markers" permission will be able to override this option.');

    if ($this->moduleHandler->moduleExists('language')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];

      $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle('marker', $this->entity->id());
      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'marker',
          'bundle' => $this->entity->id(),
        ],
        '#default_value' => $language_configuration,
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Prepares workflow options to be used in the 'checkboxes' form element.
   *
   * @return array
   *   Array of options ready to be used in #options.
   */
  protected function getWorkflowOptions() {
    $workflow_options = [
      'status' => $this->entity->getStatus(),
      'new_revision' => $this->entity->shouldCreateNewRevision(),
    ];
    // Prepare workflow options to be used for 'checkboxes' form element.
    $keys = array_keys(array_filter($workflow_options));
    return array_combine($keys, $keys);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->entity->setStatus((bool) $form_state->getValue(['options', 'status']))
      ->setNewRevision((bool) $form_state->getValue(['options', 'new_revision']));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $marker_type = $this->entity;
    $status = $marker_type->save();

    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')
      ->toString();
    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addStatus($this->t('Marker type %label has been updated.', [
          '%label' => $marker_type->label(),
        ]));
      $this->logger->notice('Marker type %label has been updated.', [
        '%label' => $marker_type->label(),
        'link' => $edit_link,
      ]);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Marker type %label has been added.', [
          '%label' => $marker_type->label(),
        ]));
      $this->logger->notice('Marker type %label has been added.', [
        '%label' => $marker_type->label(),
        'link' => $edit_link,
      ]);
    }

    $form_state->setRedirectUrl($marker_type->toUrl('collection'));
  }

}
