<?php

namespace Drupal\marker;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Provides an interface defining a marker type entity.
 */
interface MarkerTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface, RevisionableEntityBundleInterface {

  /**
   * Sets whether new revisions should be created by default.
   *
   * @param bool $new_revision
   *   TRUE if MARKER items of this type should create new revisions by default.
   *
   * @return $this
   */
  public function setNewRevision($new_revision);

}
